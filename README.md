# MonkeySheet [![Build Status](https://travis-ci.org/Pesegato/MonkeySheet.svg?branch=master)](https://travis-ci.org/Pesegato/MonkeySheet) [![](https://jitpack.io/v/Pesegato/MonkeySheet.svg)](https://jitpack.io/#Pesegato/MonkeySheet) [![Language grade: Java](https://img.shields.io/lgtm/grade/java/g/Pesegato/MonkeySheet.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/Pesegato/MonkeySheet/context:java)
Spritesheet library for jMonkeyEngine

How to use: https://www.gitbook.com/book/pesegato/monkeysheet-workflow/details

Example: https://github.com/Pesegato/MonkeySheetSampleApp
